#!/bin/sh

if hash google-chrome-stable >/dev/null 2>&1; then
  exit 0
fi

wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

SOURCELIST="/etc/apt/sources.list.d/google-chrome.list"
REPOCONFIG="deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"

if [ ! -e $SOURCELIST ]; then
  echo $REPOCONFIG | sudo tee $SOURCELIST
fi

sudo apt-get update
sudo apt-get install -y google-chrome-stable
