#!/bin/sh

if hash atom >/dev/null 2>&1; then
  exit 0
fi

ROOT=$(dirname $(dirname `readlink -f $0`))
FILE=$ROOT/tmp/atom.deb

if [ ! -e $FILE ]; then
  sh -c "wget https://atom.io/download/deb -O $FILE"
fi

sudo dpkg -i $FILE
