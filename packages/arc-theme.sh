#!/bin/sh

if [ -d "/usr/share/themes/Arc" ]; then
  exit 0
fi

sudo add-apt-repository -y ppa:noobslab/themes
sudo add-apt-repository -y ppa:noobslab/icons

sudo apt-get update
sudo apt-get install -y arc-theme arc-icons
