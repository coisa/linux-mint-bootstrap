#!/bin/sh

PHPSTORM=/opt/PhpStorm

if [ -d $PHPSTORM ]; then
  exit 0
fi

LASTBUILD="PhpStorm-2016.1.2.tar.gz"

ROOT=$(dirname $(dirname `readlink -f $0`))
FILE=$ROOT/tmp/phpstorm.tar.gz

if [ ! -e $FILE ]; then
  sh -c "wget https://download.jetbrains.com/webide/$LASTBUILD -O $FILE"
fi

USERNAME=$(whoami)

sudo tar -zxvf $FILE -C /opt
sudo chown -R $USERNAME:$USERNAME /opt/PhpStorm-*
sudo mv /opt/PhpStorm-* /opt/PhpStorm

sudo ln -s /opt/PhpStorm/bin/phpstorm.sh /usr/local/bin/phpstorm
