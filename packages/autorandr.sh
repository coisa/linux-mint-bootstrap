#!/bin/bash

if hash autorandr >/dev/null 2>&1; then
	exit 0
fi

mkdir ~/.autorandr > /dev/null 2>&1

if hash nitrogen >/dev/null 2>&1; then
	echo "#!/bin/bash" > ~/.autorandr/postswitch
	echo "nitrogen --restore" >> ~/.autorandr/postswitch
fi

git clone git@github.com:wertarbyte/autorandr.git /tmp/autorandr

sudo mv /tmp/autorandr /opt/autorandr
sudo ln -s /opt/autorandr/autorandr /usr/local/bin/autorandr