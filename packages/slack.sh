#!/bin/sh

if hash slack >/dev/null 2>&1; then
  exit 0
fi

SOURCELIST="/etc/apt/sources.list.d/slack.list"
REPOCONFIG="deb https://packagecloud.io/slacktechnologies/slack/debian/ jessie main"

if [ ! -e $SOURCELIST ]; then
  echo $REPOCONFIG | sudo tee $SOURCELIST
fi

sudo apt-get update
sudo apt-get install -y slack-desktop
