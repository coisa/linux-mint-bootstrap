#!/bin/sh

if hash fluxbox >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y fluxbox fbautostart fbpager

# backstep
