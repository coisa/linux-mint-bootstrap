#!/bin/sh

if hash variety >/dev/null 2>&1; then
  exit 0
fi

sudo add-apt-repository ppa:peterlevi/ppa
sudo apt-get update
sudo apt-get install -y variety