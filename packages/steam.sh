#!/bin/sh

if hash steam >/dev/null 2>&1; then
  exit 0
fi

if [ -e "/usr/games/steam" ]; then
  exit 0
fi

sudo apt-get install -y steam
