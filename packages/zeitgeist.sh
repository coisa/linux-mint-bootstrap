#!/bin/sh

#!/bin/sh

if hash zeitgeist-daemon >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y zeitgeist zeitgeist-explorer banshee-extension-zeitgeistdataprovider
