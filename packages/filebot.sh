#!/bin/sh

if hash filebot >/dev/null 2>&1; then
  exit 0
fi

DEB="/tmp/filebot.deb"

if [ ! -e "$DEB" ]; then
  wget -L -J -O $DEB https://app.filebot.net/download.php?type=deb&arch=amd64
fi

sudo dpkg -i $DEB

sudo apt-get install -y openjfx
