#!/bin/sh

if ! hash terminator >/dev/null 2>&1; then
  sudo apt-get install -y terminator
fi

if hash gnome-terminal >/dev/null 2>&1; then
  sudo apt-get purge -y gnome-terminal
fi

# update-alternatives --config x-terminal-emulator
