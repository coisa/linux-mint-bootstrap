#!/bin/bash

if hash mysql >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y mysql-client
