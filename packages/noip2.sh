#!/bin/sh

NOIP2="/usr/local/bin/noip2"

if [ -e $NOIP2 ]; then
  exit 0
fi

cd /usr/local/src/
sudo wget http://www.no-ip.com/client/linux/noip-duc-linux.tar.gz
sudo tar xvzf noip-duc-linux.tar.gz

cd noip-2.1.9-1/
sudo make install

# TODO descobrir como automatizar o login e a seleção automática da rede (talvez se instalar antes do docker nem pergunte)
