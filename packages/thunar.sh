#!/bin/sh

if hash thunar >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y thunar thunar-dropbox-plugin thunar-media-tags-plugin thunar-volman thunar-vcs-plugin
