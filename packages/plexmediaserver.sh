#!/bin/sh

if dpkg -s plexmediaserver >/dev/null 2>&1; then
  exit 0
fi

LASTBUILD="1.0.0.2261-a17e99e"

ROOT=$(dirname $(dirname `readlink -f $0`))
FILE=$ROOT/tmp/plexmediaserver.deb

if [ ! -e $FILE ]; then
  sh -c "wget https://downloads.plex.tv/plex-media-server/$LASTBUILD/plexmediaserver_$LASTBUILD_amd64.deb -O $FILE"
fi

sudo dpkg -i $FILE
