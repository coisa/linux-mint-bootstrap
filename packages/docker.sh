#!/bin/sh

if hash docker >/dev/null 2>&1; then
  exit 0
fi

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

SOURCELIST="/etc/apt/sources.list.d/docker.list"
REPOCONFIG="deb https://apt.dockerproject.org/repo ubuntu-xenial main"

if [ ! -e $SOURCELIST ]; then
  echo $REPOCONFIG | sudo tee $SOURCELIST
fi

sudo apt-get update
sudo apt-get install -y linux-image-extra-$(uname -r) docker-engine python-pip python-setuptools vim-syntax-docker
sudo pip install docker-compose

sudo usermod -aG docker $USER

sudo service docker start
sudo systemctl enable docker
