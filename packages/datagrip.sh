#!/bin/sh

PHPSTORM=/opt/DataGrip

if [ -d $PHPSTORM ]; then
  exit 0
fi

LASTBUILD="datagrip-2016.2.1.tar.gz"

ROOT=$(dirname $(dirname `readlink -f $0`))
FILE=$ROOT/tmp/datagrip.tar.gz

if [ ! -e $FILE ]; then
  sh -c "wget https://download.jetbrains.com/datagrip/$LASTBUILD -O $FILE"
fi

USERNAME=$(whoami)

sudo tar -zxvf $FILE -C /opt
sudo chown -R $USERNAME:$USERNAME /opt/DataGrip-*
sudo mv /opt/DataGrip-* /opt/DataGrip

sudo ln -s /opt/DataGrip/bin/datagrip.sh /usr/local/bin/datagrip
