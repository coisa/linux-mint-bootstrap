#!/bin/sh

if hash fingerprint-gui >/dev/null 2>&1; then
  exit 0
fi

sudo apt-add-repository -y ppa:fingerprint/fingerprint-gui
sudo apt-get update
sudo apt-get install -y libbsapi policykit-1-fingerprint-gui fingerprint-gui
