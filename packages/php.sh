#!/bin/sh

if hash php >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y php php-mcrypt php-intl php-xdebug php-mysql php-mbstring php-amqp \
  php-cli php-curl php-dom php-gd php-iconv php-imap php-json php-xml php-pdo phpunit

php -r "readfile('https://getcomposer.org/installer');" | sudo php -- --install-dir=/usr/local/bin --filename=composer
