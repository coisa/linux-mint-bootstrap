#!/bin/sh

if hash geary >/dev/null 2>&1; then
  exit 0
fi

sudo add-apt-repository -y ppa:geary-team/releases
sudo apt-get update
sudo apt-get install -y geary
