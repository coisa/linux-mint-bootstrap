#!/bin/sh

if ! hash tint2 >/dev/null 2>&1; then
  sudo apt-get install -y tint2
fi

if [ -e "/usr/local/bin/tintwizard" ]; then
  exit 0
fi

git clone git@github.com:luzfcb/tintwizard.git /tmp/tintwizard
sudo mv /tmp/tintwizard /opt/

sudo ln -s /opt/tintwizard/tintwizard.py /usr/local/bin/tintwizard
sudo chmod +x /usr/local/bin/tintwizard

if [ ! -d "$HOME/.config/tint2" ]; then
  mkdir $HOME/.config/tint2
fi
