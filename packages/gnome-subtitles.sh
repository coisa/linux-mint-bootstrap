#!/bin/sh

if hash gnome-subtitles >/dev/null 2>&1; then
  exit 0
fi

sudo apt-get install -y gnome-subtitles
