#!/bin/sh

if hash node >/dev/null 2>&1; then
  exit 0
fi

curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs

npm login
npm set prefix ${HOME}/.local
npm set loglevel=error
npm set progress=false
