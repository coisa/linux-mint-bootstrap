#!/bin/sh

# zenity --list --checklist --width=500 --height=500 \
# 	--title="Select packages to install" --separator=" " \
# 	--column="Install" --column="Package" --column="Description" \
# 	true "zsh" "ZSH Unix Shell" \
# 	true "oh-my-zsh" "Oh My ZSH!" \
# 	true "google-chrome" "Google Chrome Web Browser"

# TODO passar senha de sudo para os scripts `sudo -s`???
# TODO usar /tmp direto e não mais ./tmp
# TODO fazer com que nao solicite senha para nada
		# Adicionar um sudoers.d e remover depois!?
 		# `username ALL=(ALL) NOPASSWD: /path/to/script`

# alltray

for filename in $(ls sudoers); do
	filepath="/etc/sudoers.d/$filename"

	if [ ! -e $filepath ]; then
		sudo cp ./sudoers/$filename $filepath
		sudo chmod 0440 $filepath
	fi
done

for script in $(ls packages); do
	sh packages/$script
	cd $(pwd)
done
